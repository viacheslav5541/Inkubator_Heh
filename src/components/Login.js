import React from 'react';
import { connect } from 'react-redux'
import { Alert, StyleSheet, Text, TextInput, Button, View , AsyncStorage } from 'react-native';
import { Actions } from '../actions/Actions'

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    };
  }

  static navigationOptions = {
    headerStyle: {
      backgroundColor: "aquamarine",
      elevation: null,
    },
  };

  onBtnAuth() {
    
    const { username, password } = this.state;
    const { dispatch } = this.props;
    if (username && password) {
     dispatch(Actions.login(username, password))
    } else {
      Alert.alert('Поля не могут быть пустыми')} 
  }
  
  render() {
    let {login}  = this.props
    console.log('logged', login)

    if (this.props.login){
        let {navigate} = this.props.navigation
        return navigate('NewsScreen', {})
    }

    return (
      <View style={styles.container}>
        <Text style={styles.header}>NEWS</Text>
        <Text>Авторизация</Text>
        <TextInput
          style={styles.inputbox}
          placeholder="Имя пользователя"
          onChangeText={(username) => this.setState({username})}
        />
        <TextInput
          style={styles.inputbox}
          placeholder="Пароль"
          secureTextEntry
          onChangeText={(password) => this.setState({password})}
        />
        <View style={styles.buttonContainer}>
          <Button
            onPress={this.onBtnAuth.bind(this)}
            title="Войти"
            color="red"
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'aquamarine',
  },
  header: {
    color: '#f1841e',
    fontWeight: 'bold',
    fontSize: 30,
    paddingTop: 50,
    marginBottom: 110,
    shadowColor: '#f8983f',
  },
  inputbox: {
    padding: 5,
    width: 200,
  },
  buttonContainer: {
    marginTop: 7,
    marginBottom: 5,
  },
});

 
function mapStateToProps(state) {
 const { login } = state.login;
  return {
   login,
  };
}

const connectedLoginPage = connect(mapStateToProps)(Login);
export default connectedLoginPage;
