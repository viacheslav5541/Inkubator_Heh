import {createStackNavigator} from 'react-navigation'

import Login from '../components/Login'
// import News from '../components/News'
// import Profile from '../components/Profile'
// import SelectNews from '../components/SelectNews'

const Navigation = createStackNavigator({
  LoginScreen: { screen: Login },
//   NewsScreen: { screen: News },
//   ProfileScreen: { screen: Profile },
//   SelectNews: { screen: SelectNews },
})

export default Navigation;
