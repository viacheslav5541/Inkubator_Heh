import { LOGIN_SUCCESS } from '../constants/Constants';

const initialState = {
  login: false,
};

export function login(state = initialState, action) {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {...state, login: true};
    default:
      return state
  }
}
