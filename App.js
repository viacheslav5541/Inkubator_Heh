import React from 'react'
import { Provider } from 'react-redux'
import App from './src/containers/App'
import configureStore from './src/store/configureStore'
import { AppRegistry } from 'react-native'

const store = configureStore()

const redux = () => (
  <Provider store={store}> 
    <App />
  </Provider>
)

export default redux

AppRegistry.registerComponent('redux', () => redux)
